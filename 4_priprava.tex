\section{Návrh řídicího zařízení} % (fold)
\label{sec:teorie a návrh}

Zařízení pro řízení technologických procesů na tokamaku GOLEM má pracovat ve dvou režimech:
\begin{enumerate}
	\item Předdefinovaný
	\item Zpětnovazební
\end{enumerate}

V předdefinovaném režimu se počítá s tím, že všechny události a průběhy budou napočítané před výbojem. Připraví se tzv. waveform - časový průběh veličin ovládací technologie. Na skutečný stav na tokamaku v průběhu výboje nebude brán ohled.

Ve druhém, real-time, režimu se počítá s tzv. zpětnovazební řídicí smyčkou. Waveformy se připraví pro požadované veličiny plazmatu, ne pro ovlivňovací systémy. Ovládací technologie bude v průběhu výboje měřit a vyhodnocovat data, podle toho bude nastavovat veličiny ovládacích technologií, aby řízené veličiny co nejlépe odpovídaly nastaveným waveformám.


Systém zpětnovazebního řízení procesu obvykle sestává z těchto tří komponent: 
\begin{itemize}
	\item Diagnostika v reálném čase (nazývaná také snímač, senzor)
	\item Řídicí schéma (také regulátor)
	\item Aktuátor - ovlivnění řízeného systému (také akční člen)
\end{itemize}


Pro vstřikování plynu - ,,gas puffing'' se uvažuje provoz v předdefinovaném režimu\footnote{Byly probírány i možnosti zpětnovazebního řízení hustoty plazmatu, po úvaze pak byly odloženy.}. Pro ovlivňování a stabilizaci polohy plazmatu jsou uvažovány oba tyto režimy.

V průběhu zpětnovazebního řízení v reálném čase do systému vstupuje reference - požadovaný stav. V případě řízení polohy je to požadovaná poloha prstence plazmatu. V každý okamžik se vyhodnocuje rozdíl hodnoty naměřené od požadované. Regulační odchylka (angl. ,,measured error'') je vstupním parametrem pro řídicí schéma (angl. ,,controller''). Řídicí schéma určí vstup pro aktuátor, který ovlivňuje systém - v našem případě stabilizační cívky. Schéma zpětnovazební řídicí smyčky je na Obr. \ref{fig:feedback}.


\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=0.65\textwidth]{img/1024px-Feedback_loop_with_descriptions.png}
	\end{center}
	\caption{Zpětnovazební řídicí smyčka. Převzato z \cite{wiki_feedback_pic}}
	\label{fig:feedback}
\end{figure}

\subsection{Diagnostika a aktuátor} % (fold)
\label{sub:diagnostika_a_aktuátor}



Pro zpětnovazební řízení polohy v reálném čase je třeba zvolit vhodnou diagnostiku.
Rychlé kamery nejsou vhodné. Vyhodnocovací jednotka kamer je součást přístroje fotoaparátu a není prakticky možné vyčítat data v reálném čase.
Magnetickou diagnostiku lze k určení polohy plazmatu použít. Magnetiku využíval např. systém J. Kocmana \cite{Jindra_DP}. Využití magnetické diagnostiky mimo jiné vyžaduje v reálném čase kompenzovat signály od jiných magnetických polí (třeba $B_\mathrm{t}$). V případě využití Mirnovových cívek také integraci a tedy případné nulování offsetu vyhodnocovacího zařízení.
Dále je možné využít signál z bolometrů.  Rutinně se na GOLEMu zaznamenává kompletní signál z AXUV20EL pomocí datových sběrů Papouch DAS, nicméně signál z bolometrů je možné odbočit a použít pro vyhodnocení polohy v reálném čase. Výhodou použití bolometrů oproti magnetické diagnostice je, že není třeba provádět integraci signálu a není třeba eliminovat signál z ostatních magnetických polí. Dále by bylo možné pomocí bolometrů případně zjišťovat nejen polohu, ale i jiné charakteristiky profilu plazmatu. Nevýhodou je, že tento způsob není běžně používaný a vyzkoušený; dále je také třeba zpracovat více kanálů signálu.


Jako aktuátor pro stabilizaci a ovlivnění polohy plazmatu ve vertikálním směru se na GOLEMu využívají čtveřice toroidálně vinutých cívek (Obr. \ref{fig:stab_civky_v} ). Z teorie plazmatu plyne, že v silném magnetickém poli tokamaku budou kolektivní drifty plazmatu malé a bude tedy vhodný mikroskopický popis plazmatu jako pohyb nabitých částic ve vnějším magnetickém poli.\cite{chen}
Rychlost jednotlivých částic je vždy primárně toroidální a tedy téměř kolmá k ovlivňovacímu poli a účinek stabilizačního pole bude přibližně odpovídat magnetické indukci generované stabilizačními cívkami a tedy velikosti elektrického proudu protékajícímu stabilizačními cívkami. Lze tedy napsat proporce pro mikroskopickou sílu působící na jednotlivou částici $f_\mathrm{stab_\mathrm{m}} $, popř. pro pohyb jednotlivé částice v ose $z$ (cylindrické souřadnice, kapitola \ref{sub:geometrie_tokamaku})
\begin{equation}
	f_\mathrm{stab_\mathrm{m}} \propto q \cdot B_\mathrm{stab} \cdot v_\mathrm{t}
	\qquad \qquad
	\ddot z_\mathrm{m} \propto \frac{ B_\mathrm{stab} \cdot v_t \cdot q }{m}
	\label{eq:umernost_f_I}
\end{equation}
kde $ B_\mathrm{stab}$ je velikost stabilizační magnetické indukce, $q$ náboj částice, $m$ její hmotnost a $v_\mathrm{t}$ její složka rychlosti v toroidálním směru. V tokamaku jsou elektrony a ionty elektrickým polem urychleny opačně a tedy oba budou daným stabilizačním polem vychylovány shodně.\footnote{Zrychlení elektronů je vyšší kvůli podstatně vyššímu $q/m$. Nicméně vychýlení jednoho druhu částic vyvolává vnitřní elektrické pole plazmatu, které toto kompenzuje.} Lze pak tedy napsat přibližnou makroskopickou rovnici pro zrychlení prstence plazmatu ve vertikálním směru $\ddot z$
\begin{equation}
	\ddot z \propto \frac{I_\mathrm{p} B_\mathrm{stab} }{m_\mathrm{p}} \propto \frac{I_\mathrm{p}I_\mathrm{stab}}{V_p \cdot \overline{n}}
	\label{eq:stabilizace_aktuator}
\end{equation}
kde $I_\mathrm{stab}$ je proud protékající stabilizačními cívkami, $\overline{n}$ je střední hustota plazmatu a $V_p$ je objem plazmatu. Z \eqref{eq:stabilizace_aktuator} lze vidět, že zrychlení odpovídá lineárně proudu protékajícímu stabilizačními cívkami.

% subsection diagnostika_a_aktuátor (end)

\subsection{Schéma řízení} % (fold)
\label{sub:schéma_řízení}
Schéma řízení obecně převádí vývoj odchylky na vývoj vstupu pro řízený systém; je třeba jej volit podle chování aktuátoru a systému. Pro obecnou stabilizaci plazmatu v tokamaku je možné použít velice komplexní řídicí schémata založená na různých modelech plazmatu a predikci budoucího vývoje \cite{GERKSIC2018158,Roseline:2016:1546-1955:9057}. 

Z technické praxe je dobře známé schéma tzv. PID regulátoru. \cite{PID_automa} Ten lze matematicky vyjádřit jako
\begin{equation}
	\begin{split}
		O(t) &= \alpha P(t) + \beta In(t) + \gamma D(t) \\
		P(t) &= \Delta z(t)\\
		I(t) &= \int_{0} ^{t} \Delta z(\tau) d\tau\\
		D(t) &= \frac{\textrm{d} \Delta z}{\textrm{d} t}(t)\\
	\end{split}
	\label{eq:PID}
\end{equation}
$O$ je zde výstup regulačního schématu a $\Delta z$ odchylka od požadované polohy plazmatu.
PID regulátor počítá časovou derivaci a integrál vstupního signálu. Parametry $\alpha, \beta \mathrm{ a } \gamma$\footnote{Často se lze setkat s vyjádřením $T_i = \beta^{-1}$ jako s časovou konstantou integrační složky.} popisují poměr jednotlivých složek a tvoří nastavení regulátoru. Bylo by možné regulátor zobecnit např. libovolnými funkcemi namísto koeficientů.
Takovéto regulační schéma je vhodné pro řízení systému, u kterého je možné popsat chování diferenciální rovnicí ve veličině aktuátoru. Správným nastavením PID regulátoru je možné udržovat systém v jinak nestabilní poloze. Hojně se využívá v průmyslu, nebo např. v populárních dronech. Pro stabilizaci polohy plazmatu v tokamaku GOLEM by mělo být PID schéma vhodné.

Experimentálně bylo zjištěno, že pro stabilizaci polohy plazmatu v tokamaku ve vertikálním směru je možné uvažovat, že síly, které prstenec plazmatu vychýlí, jsou větší, než síly vyvolané setrvačností. Místo pohybové rovnice pro prstenec plazmatu \eqref{eq:stabilizace_aktuator} je vhodné uvažovat vyrovnání náhodně vzniklé síly.\cite{rizeni_prehled} Pro stabilizaci polohy plazmatu by mělo být dostatečné i jednodušší, čistě proporcionální, regulační schéma.\cite{rizeni_prehled} To lze popsat
\begin{equation}
	O(t) = -k \cdot z(t)
\end{equation}
a je to tedy speciální případ \eqref{eq:PID} s nulovou integrační i derivační složkou. Jeho nevýhodou je, že takový regulátor vykazuje tzv. trvalou regulační odchylku.


% subsection schéma_řízení (end)

\subsection{Zařízení pro řídicí systém} % (fold)
\label{sub:zařízení_pro_řídicí_systém}

Pro výpočetní elektroniku, která ovládá procesy v průběhu výboje, je vhodné, aby pracovala s deterministickými zpožděními a reagovala velmi rychle.


Běžně používané počítače s operačním systémem vypadají na první pohled příhodně - je zde jednoduchá implementace komunikace po ethernetu a vývoj softwaru by byl snadný. Také umí třeba zpracovávat sekvenčně velké množství dat. Bohužel ale zpravidla nepočítají s přísnými časovými nároky. Kupříkladu běžný PC s OS Windows je schopný na události reagovat řádově v jednotkách až desítkách milisekund, GNU/Linux s RT jádrem a vhodným schedulerem pak v desetinách až jednotkách. Existují speciální operační systémy pro práci v reálném čase (např. RTEMS, VxWorks). Na něm je založena většina zařízení pro řízení velkých tokamaků (kapitola \ref{sub:řízení}). Také  Bývalý stabilizační systém běžel na počítači NI PXI s procesorem Intel Core2 a operačním systému VxWorks.\cite{Jindra_DP} Nevýhodou takovéhoto řešení je pak zpravidla omezená podpora hardwaru (počítače a hlavně datových vstupů a výstupů). Vhodný hardware pro rychlé analogové vstupy a výstupy je velmi drahý.

Deterministického časování i malého zpoždění by bylo možné dosáhnout, kdybychom místo počítače s operačním systémem zvolili překlad kódu rovnou pro procesor.  To se nejčastěji používá pro relativně jednoduchou elektroniku (třeba osmibitové mikroprocesory Atmel AVR), ale i pro výkonnější systémy (třeba 32 bit ARM Cortex M0). Takové systémy jsou vhodné pro řízení jednoduchých procesů na experimentu - např. generátory synchronizačních impulsů, nebo jako interlock. Pro výpočty v reálném čase by takovéto mikroprocesory vhodné nebyly - neposkytují dostatek prostředků.

Nejrychlejší možnost je tzv. programovatelná logika. Zde se již nejedná o programování sekvence kroků, jako např. u běžných počítačů. Zpravidla se programováním rozumí nastavení matice logických hradel. Programováním se tedy nastavuje samotný hardware. S náročností zpracovávaných operací se tedy nezvyšuje čas výpočtu, ale nároky na samotný hardware. 


Nejjednodušší zařízení, které by se dalo považovat za programovatelnou logiku je už PROM paměť. Tam se ,,programují'' (spojují/přerušují) vstupy do řady logických součtových hradel. \cite{abc}
Komplikovanější jsou tzv. PLD (Programable Logic Devices). Ty zpravidla mívají programovatelnou matici vstupních součinových hradel místo adresového dekodéru následovanou opět OR hradly a nejčastěji i možnost zavést výstupy do vstupů.
Tím se jejich možnost např. rozšiřuje o sekvenční zpracování.
Takzvaná komplexní programovatelná logika (CPLD) zobecňuje PLD. Jednotlivé prvky se sdružují do tzv. buněk a ty vzájemně se programováním propojují.
Nejobecnější ze všech je FPGA (Field Programable Gate Array, česky hradlové pole). 
To v sobě obsahuje samotné bloky programovatelné logiky a vstupů/výstupů propojeny komplexní programovatelnou maticí, ale nejčastěji i blokové paměti nebo bloky pro aritmetické operace. \cite{farooq2012fpga}. Zpracování dat na FPGA neprobíhá sekvenčně, jako na klasickém procesoru, nýbrž může probíhat paralelně.

\newcommand{\pitaya}{\mbox{Red~Pitaya~STEMLab~125-10}}
Pro realizaci zařízení k řízení procesů na tokamaku v reálném čase byl vybrán počítač \pitaya, který kombinuje výše popsaný běžný počítač a programovatelnou logiku FPGA.
% subsection zařízení_pro_ovládací_systém (end)

\subsection{\pitaya} % (fold)
\label{sub:pitaya}
\pitaya\ je multifunkční přístroj pro vývoj a testování elektroniky s ovládáním přes počítač. \pitaya\ se připojuje do počítačové sítě a ovládá se prostřednictvím webového rozhraní. Systém přístroje \pitaya\ umožňuje spouštět různé ,,virtuální přístroje'', jako např. osciloskop, funkční generátor, logický analyzátor, LCR meter nebo impedanční analyzátor.


\pitaya\ je zařízení založené na čipu Xilinx ZYNQ 7010. Ten v sobě kombinuje ARM procesor a programovatelnou logiku FPGA. V kontextu systému Xilinx se těmto součástem říká PS (Processing System) a PL (Programable Logic). PL a PS spolu komunikují prostřednictvím AXI4 rozhraní \cite{XilinxAXI}

PS v případě Xilinx ZYNQ 7010 obsahuje 2 jádra ARM Cortex-A9. Na desce \pitaya\ je k němu pak připojeno 256 MB RAM a jsou vyvedena tato rozhraní:
\begin{enumerate}
 	\item Gigabitový ethernet
 	\item MicroSD paměťová karta
 	\item USB
 	\item sériová konzole
 \end{enumerate}
 Na přístroji \pitaya\ není rozhraní pro připojení monitoru. Na PS běží operační systém GNU/Linux a zajišťuje komunikaci a ovládání. Dále PS funguje jako programátor pro PL, tedy za běhu systému  je možné nahrát do PL požadovaný tzv. \textit{bitstream}. To je obdoba programu pro FPGA. Bitstream obsahuje kompletní nastavení matice hradel. (více v \ref{sub:programování_fpga}).

PL poskytuje \cite{XilinxZYNQ}
\begin{itemize}
	\item 28 k logických buněk
	\item 2.1 MB blokové RAM
	\item 80 DSP slices (bloků pro výpočty používané ke zpracování signálu)
	\item až 100 pinů digitálních IO
\end{itemize}

Přístroj \pitaya\ je dále vybaven \cite{PitayaSTEMlab}
\begin{itemize}
	\item 2x 10 bit ADC 125 Ms/s
	\item 2x 10 bit DAC 125 Ms/s
	\item 4x 10 bit ADC 100 ks/s
	\item 4x 10 bit DAC 100 ks/s\footnote{Udávaný údaj platný pro zamýšlený OS a ,,ekosystém''. Reálně jsou pomalé DAC realizovány jako PWM na úrovni FPGA, proto jich teoreticky může být i více nebo méně.}
	\item 16 uživatelsky dostupných GPIO
	\item Digitální rozhraní SPI, I$^2$C a UART
\end{itemize}
přičemž všechna tato rozhraní jsou zapojena do PL.

Rychlé analogové I/O jsou vyvedeny na čtveřici koaxiálních SMA konektorů, pomalé pak na header E1.
Nákres desky se součástkami je na Obr. \ref{fig:pitaya}.


Při zamýšleném použití běží na PS Linuxový systém založený na distribuci Debian. Pod ním běží webserver Nginx, který poskytuje veškeré ovládací rozhraní. Při spuštění virtuálního přístroje je na PS spuštěn obslužný nativní program (zpravidla psaný v C) a zároveň do PL nahrán příslušný bitstream.
Při provozu virtuálních měřicích přístrojů jsou rozhraní obsluhována PL. Někdy na PL probíhá i část zpracování signálu. Signál je poté předán obslužnému programu. Ten vygeneruje výstupy pro uživatele (často ve formě grafu) a odešle na webovou stránku.

Hardware \pitaya\ není open source, avšak je relativně dobře zdokumentovaný. Většina softwaru pro \pitaya\ je open source.
To umožňuje využití přístroje i pro jiné účely, než pro vývoj vlastního hardwaru. \pitaya\ je často využíván např. pro experimenty s rádiovým přenosem na dlouhých vlnách (SDR), nebo pro řízení fyzikálního experimentu na synchrotronu SOLEIL \cite{PitayaSOLEIL}.

\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.6\textwidth]{img/pitaya_board.jpg}
	\end{center}
	\caption{Součásti desky Red Pitaya STEMLab. Převzato z \cite{pitaya_web}}
	\label{fig:pitaya}
\end{figure}

% subsection pitaya (end)

\subsection{Programování FPGA} % (fold)
\label{sub:programování_fpga}
Proces programování FPGA je značně odlišný od programování klasických počítačů. Místo programovacích jazyků se používají tzv. HDL (Harware Description Language).

Podobně, jako se u počítačů řeší ,,úrovňovost programování'' (assembler, kompilované jazyky, interpretované jazyky), tak u FPGA lze programovat na behaviorální úrovni, register transfer úrovni (RTL) až po úroveň nastavování propojení jednotlivých hradel. Pro použití na tokamaku je uvažován RTL návrh.

RTL návrh je schéma abstrakce, ve kterém jsou data přesouvána mezi hardwarovými registry. Je k dispozici výšeúrovňovější přístup, než při ručním návrhu na úrovni hradel. Jednotlivé bloky mohou obsahovat kombinační i sekvenční logiku a zpracování je synchronní. Nejčastěji se navrhuje v HDL jazycích Verilog nebo VHDL. \cite{VHDL-Verilog} Jednotlivé ucelené funkční bloky se nazývají RTL moduly.
Často jsou distribuovány ucelené jednotky logiky, jejichž návrh je duševním vlastnictvím autora. Ty se nazývají IP cores. \cite{wiki_ipcores}

RTL návrh prochází procesem zvaným \textit{syntéza}. Syntetizovaný návrh je popsaný na úrovni propojení jednotlivých hradel. Je technologicky podobný takzvanému netlistu, který je známý z návrhu klasické elektroniky.
Syntetizovaný návrh je možné např. simulovat na počítači. Pro použití na reálném FPGA je potřeba provést tzv. \textit{implementaci}. Při té je uvažována struktura konkrétního FPGA čipu a návrh je převeden na úroveň buněk a sliců. Po implementaci lze vygenerovat tzv. \textit{bitstream}, který už lze nahrát do FPGA čipu.

Pro čip Xilinx ZYNQ 7010 nabízí výrobce vývojovou sadu Xilinx Vivado, ve variantě webpack zdarma. Ta umožňuje psát vlastní HDL kód, používat připravené IP cores, syntetizovat, v jisté míře i provádět simulace, implementovat pro podporované čipy i generovat bitstream.
Celý softwarový balík je velmi komplexní. Má funkcionalitu "IP integrator", která dokonce umožňuje graficky propojovat jednotlivé připravené IP cores a vlastní RTL bloky kódu. Dále nabízí nástroje pro správu a programování hardwaru a v neposlední řadě také nástroje pro generování systému pro PS. 


V rámci FPGA je nutné řešit časování. V základním režimu uvažovaném na \pitaya\ pracuje PL na frekvenci 125 MHz. Každý cyklus je tedy k dispozici jeden sample z rychlých kanálů ADC, je možno nastavit DAC a je možnost přistoupit k digitálnímu I/O.\footnote{Reálně je připojení DAC řešeno složitěji. \cite{Pitayadocs}}
% subsection programování_fpga (end)

% section teorie a návrh (end)

\clearpage
