

\section{Tokamak} % (fold)
\label{sec:tokamak}
Zařízení typu tokamak (z ruského ,,toroidální komora v magnetických cívkách'') využívá toroidální magnetické pole jako ,,magnetickou nádobu'' pro plazma. 

Komora tokamaku s kruhovým průřezem komory je geometricky jednoduchý torus, tedy ,,stočený válec''. 

\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.6\textwidth]{img/Tokamak-pole.pdf}
	\end{center}
	\caption{Ilustrativní nákres cívek a magnetických polí v zařízení typu tokamak. Převzato z \cite{tokamak_fields_img}, přeloženo.}
	\label{fig:tokamak_pole}
\end{figure}


Kolem komory jsou umístěny cívky vytvářející udržující toroidální magnetické pole $B_t$. V průběhu provozu může toto magnetické pole zůstávat stálé.

Siločáry tohoto pole jsou prstence s osou odpovídající hlavní ose toru. Směrem ke středu jsou ale cívky blíže sobě a velikost magnetické indukce je tedy vyšší. To v plazmatu způsobuje tzv. toroidální drift, který by vychýlil částice ze stabilní uzavřené trajektorie. Ten lze kompenzovat více způsoby. Buď lze použít složitější konfiguraci s rotační symetrií (pak se hovoří o tzv. stelarátoru), nebo lze uvažovat, že se vybudí elektrický proud  prstencem plazmatu, vytváří vlastní magnetické pole a výsledné magnetické pole je tedy helicidní.

Existuje více způsobů vybuzení proudu plazmatem. Prakticky na všech tokamacích se využívá transformátoru.
Kvůli tomu jsou kolem nebo skrz komoru vedeny tzv. cívky primárního vinutí. Ty vytváří elektrické pole, které generuje proud plazmatem. Více v \ref{sub:elektromagnetické_pole_v_tokamaku}.

\subsection{Geometrie tokamaku} % (fold)
\label{sub:geometrie_tokamaku}
Nákres geometrie je na Obr. \ref{fig:tokamak_geometrie}.


Osa toru (rotační symetrie celého tokamaku), se nazývá hlavní osa tokamaku. Komora je ve svém průřezu lokálně symetrická podle středu. Ty spojuje kružnice nazývaná vedlejší osa. Otáčení podle hlavní osy se nazývá toroidální, otáčení podle vedlejší se nazývá poloidální.

Rozměry tokamaku určuje tzv. hlavní poloměr $R_0$: vzdálenost vedlejší osy od hlavní osy a vedlejší poloměr $r_0$: poloměr kolmého průřezu komory.

K orientaci v tokamaku lze používat několik souřadnicových systémů. Kartézské souřadnice nerespektují žádnou osu symetrie. Hlavní osu respektují klasické cylindrické souřadnice $(R, \varphi, z)$
\begin{equation}
	\begin{split}
	x &=R \cdot \cos \varphi \\
	y &=R \cdot \sin \varphi \\
	z &=
z	\end{split}
	\label{eq:cylindricke}
\end{equation}
Důsledek toho je, že pohyb v toroidálním směru lze popsat jedním parametrem. 

Pro popis tokamaku se často používají pseudocylindrické, neboli toroidální souřadnice $(r, \varphi, \theta)$
\begin{equation}
	\begin{split}
	x &=R \cdot \cos \varphi +r \cdot  \cos \theta \\
	y &=R \cdot \sin \varphi +r \cdot  \cos \theta \\
	z &=r \cdot \sin \theta
	\end{split}
	\label{eq:pseudocylindricke}
\end{equation}
Ty respektují hlavní i vedlejší osu toru. V popisu těchto souřadnic pak např. částice letící po helicidní trajektorii lze popsat dvěma parametry, které odpovídají toroidálnímu a poloidálnímu úhlu.



\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.75\textwidth]{img/geometrie.pdf}
	\end{center}
	\caption{Nákres rozměrů a používaných souřadnicových systémů v tokamaku.}
	\label{fig:tokamak_geometrie}
\end{figure}

% subsection geometrie_tokamaku (end)

\subsection{Elektromagnetické pole v tokamaku} % (fold)
\label{sub:elektromagnetické_pole_v_tokamaku}


Toroidální pole $B_t$ bývá zpravidla generováno sadou shodných cívek navinutých kolem komory. $B_t$ bývá na zařízeních typu tokamak udržováno v řádu desetin až jednotek T. Většina dnešních zařízení používá cívky z mědi. Elektrický proud pro udržení takto silného pole však cívky intenzivně zahřívá a silně komplikuje dlouhodobější provoz. Navíc zvyšuje energetickou náročnost provozu zařízení. Proto se u nových tokamaků (třeba ITER) uvažují cívky supravodivé.


Při kvantitativní rozvaze toroidálního pole bude plynout z první Maxwellovy rovnice (Ampérova zákona)
\begin{equation}
	\begin{split}
		|\vec{B}_\mathrm{t}|& = \frac{\mu_0 n I_\mathrm{t}}{2 \pi} \cdot \frac{1}{R}
	\end{split}
	\label{eq:tor_B}
\end{equation}
kde $n$ je počet závitů, $I$ proud toroidálními cívkami a $R$ podle kapitoly \ref{sub:geometrie_tokamaku} vzdálenost od hlavní
osy. 
Ze vztahu \eqref{eq:tor_B} pak plyne, že směrem k vnitřní stěně se intenzita pole zvyšuje. Proto se používá značení HFS (High Field Side) pro vnitřní stranu a LFS (Low Field Side) pro vnější.


Cívky primárního vinutí můžou mít více forem. U starších tokamaků se používalo transformátorové EI jádro, které tvořilo magnetický obvod. U moderních tokamaků byly požadavky takové, že kvůli saturaci prakticky nebylo možné používat feromagnetické jádro. Častěji se používá konfigurace centrálního solenoidu, při které je cívka navinuta kolem hlavní osy tokamaku.


Toto magnetické pole se neuvažuje statické, ale časově proměnlivé. Aplikací Maxwellovy rovnice \footnote{Chen a wikipedie ji uvádí jako druhou, Štoll jako třetí. Ale Štoll dává menší důraz na číslování.} - Faradayova indukčního zákona
\begin{equation}
	\nabla \times \vec E_{\mathrm{cd}} = - \frac{\partial \vec B_\mathrm{p}}{\partial t}
	\label{eq:maxwell2}
\end{equation}
kde $B_p$ je poloidální magnetické pole a $E_\mathrm{cd}$ výsledné budící nezřídlové elektrické pole. Napětí, které se utvoří na jednom poloidálním závitu vodiče, budeme nazývat $U_\mathrm{loop}$

Prstencem plazmatu (či jakoukoli elektricky vodivou smyčkou) tedy bude protékat proud a bude generovat poloidální magnetické pole $B_\mathrm{p}$. Magnetická pole $B_\mathrm{t}$ a $B_\mathrm{p}$ se superponují a výsledné magnetické  pole bude helicidní. Ilustrace na Obr. \ref{fig:tokamak_pole}.
Primární vinutí se těmto cívkám říká, protože odpovídají primárnímu vinutí transformátoru. Sekundární by tvořil právě prstenec plazmatu závit nakrátko.

Indukce elektrického proudu je důvod, proč se o tokamaku dnes přemýšlí jako o pulzním zařízení. Pro udržení elektrického pole $E_\mathrm{cd}$ neomezenou dobu by musel růst proud primárním vinutím nade všechny meze.

Tento indukovaný proud není jediný zdroj elektrického proudu plazmatem. Důležitou roli hraje tzv. bootstrap proud \cite{Kessel_1994}. Ten dává jistou naději na principiální možnost kontinuálního provozu tokamaku. 

Pro tokamak se označuje míra helicity celkového magnetického pole jako tzv. Bezpečnostní faktor \footnote {angl. safety factor \cite{iter_glossary}} $q$. Bezpečnostní z pohledu stability (a tedy ,,bezpečnosti'' vůči disrupcím). Definuje, resp. počítá se jako
\begin{equation}
	q=\frac{\textrm{ \# toroidálních oběhů}}{1 \textrm{ poloidální oběh}}, 
	\quad
	q=\frac{\textrm{d} \Phi}{\textrm{d} \psi}
\end{equation}
kde $\Phi$ je magnetický indukční tok v toroidálním směru a $\psi$ v poloidálním. Faktor $q$ není pro celou oblast plazmatu konstantní. Většinou se na tokamacích plánuje střední hodnota $1 < q < 3$


Obecně se na tokamacích používají další sady cívek v různých konfiguracích pro tvarování a manipulaci s plazmatem. Pro tuto práci je relevantní poloidální magnetické pole pro ovlivňování polohy plazmatu. V malých tokamacích (třeba na tokamaku GOLEM) lze takové pole realizovat pomocí dvou čtveřic cívek - jedné pro horizontální a jedné pro vertikální směr. Jednotlivé cívky střídají orientaci a výsledné pole v samotné komoře pak je přibližně homogenní. Nákres takových cívek a magnetického pole je na Obr. \ref{fig:stab_civky_h}, \ref{fig:stab_civky_v}. 

\begin{figure}[ht]
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{img/stab_civky_h.png}
		\caption{Vertikální magnetické pole pro ovlivnění horizontální polohy}
		\label{fig:stab_civky_h}
	\end{subfigure}
	\hspace{0.5cm}
	\begin{subfigure}[b]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{img/stab_civky_v.png}
		\caption{Horizontální magnetické pole pro ovlivnění vertikální polohy}
		\label{fig:stab_civky_v}
	\end{subfigure}
	\caption{Nákres cívek poloidálního magnetického pole pro ovlivňování polohy plazmatu. Převzato z \cite{Jindra_DP}}
\end{figure}

\subsection{Ohřev} % (fold)
\label{sub:ohřev}
Magneticky udržované plazma je třeba dále ohřát na fúzní teplotu.

Již zmíněný proud plazmatem také předává do plazmatu energii - ohmický ohřev plazmatu. Elektrický výkon ohřevu plazmatu $P_\mathrm{ohm}$ pak lze spočíst
\begin{equation}
	P_\mathrm{ohm} = U_\mathrm{loop} \cdot I_\mathrm{p}
\end{equation}
což je ovšem zavádějící, neboť $U_\mathrm{loop}$ zdaleka není konstantní a s proudem plazmatu znatelně poklesne. Z ohmova zákona lze dosadit
\begin{equation}	
	P_\mathrm{ohm} = R_\mathrm{p} \cdot I_\mathrm{p}^2
\end{equation}
kde $R_\mathrm{p}$ je elektrický odpor prstence plazmatu. Ten podle Spitzerovy formule \cite{Spitzer} odpovídá elektronové teplotě plazmatu $T_\mathrm{e}$
\begin{equation}
	R_\mathrm{p} \propto T_\mathrm{e} ^{- \frac{3}{2} }
\end{equation}
A pro tepelný výkon ohmického ohřevu tedy platí
\begin{equation}
	P_\mathrm{ohm} \propto T_\mathrm{e} ^{- \frac{3}{2} } \cdot I_\mathrm{p}^2
\end{equation}
neboli roste s druhou mocninou proudu, avšak klesá s $\tfrac{3}{2}$ mocninou elektronové teploty.
Vléct příliš velký proud plazmatem je nevýhodné (mimo jiné kvůli MHD nestabilitám \cite{wesson}) a proto je ohmický ohřev vhodný pouze pro nízké teploty plazmatu.

Dalším způsobem, který se využívá, je ohřev pomocí elektromagnetické vlny. Nejčastěji se využívá tzv. iontový cyklotronní rezonanční ohřev (ICRH) a elektronový cyklotronní rezonanční ohřev (ECRH). Tyto ohřevy využívají, že v magnetickém poli tokamaku nabité částice přirozeně konají cyklotronní pohyb, při rezonanci s elektromagnetickou vlnou jim lze předat energii. Při ICRH se využívá rádiových frekvencí (desítky MHz). Ty bývají zpravidla generovány elektronikou a zesilovány elektronkovými zesilovači. Vlny bývají vedeny pomocí koaxiálního kabelu a vyzářeny pomocí speciální antény v komoře tokamaku. Taková vlna vyzářená anténou by měla ve vakuu vlnovou délku jednotek až desítek metrů. Proto se nepočítá s propagací energie do hloubky komory, ale s ohřevem vnější vrstvy plazmatu. ICRH ohřívá primárně ionty izotopů vodíku na vyšších harmonických, popř. základní frekvenci minoritních prvků. ECRH oproti tomu pracuje s frekvencemi stovek GHz. Vlny ECRH jsou generovány pomocí gyrotronů \footnote{speciálních elektronek} a vedeny pomocí vlnovodů. Při těchto frekvencích jsou vlnové délky v řádu milimetrů. Návrhem antény je tedy možné ovlivňovat směr vyzařování a tedy oblast ohřevu plazmatu. Uvažuje se i o vlně s frekvencí odpovídající toroidálnímu oběhu, která by mohla neinduktivně budit proud plazmatem.

Další používanou technologií jsou svazky neutrálních částic (NBI). Princip neutrálních částic spočívá ve vstříknutí částic o vysoké energii tangenciálně s vedlejší osou tokamaku. Neutrální částice jsou generovány klasickým urychlením iontů a následnou neutralizací. Rovnou ionizované částice by pro tento účel nemohly být použity, neboť by svazek nabitých částic byl v magnetickém poli tokamaku odchýlen dříve, než by dosáhl plazmatu. Výhodou NBI je, že generuje nadenergetické částice. Pomocí NBI je také možné dostat případné fúzní palivo do centra plazmatu.

% subsection ohřev (end)

\subsection{Pracovní plyn a palivo} % (fold)
\label{sub:pracovní_plyn_a_palivo}
Pro operaci tokamaku je nutné udržovat hustotu částic. U magneticky udržované fúze se uvažují relativně nízké hustoty. Není výhodné mít v plazmatu částice s vysokým protonovým číslem, takové nečistoty podstatně zvyšují ztráty energie plazmatu brzdným zářením \cite{Mlynar}. V praxi bývá komora tokamaku před výbojem evakuována na vysoké vakuum. To se provádí vícestupňovým čerpáním - nejčastěji pomocí rotačních olejových vývěv a turbomolekulárních vývěv\footnote{U nových tokamaků, např. ITER, se uvažuje čerpání kryovývěvami.}. Před samotným výbojem se napouští pracovní plyn pomocí ventilů zpravidla na tlak v řádu desítek až stovek mPa.

V průběhu trvání fúzních reakcí pak bude třeba 
\begin{itemize}
	\item Doplňovat fúzní palivo
	\item Odvádět fúzní produkty
\end{itemize}
Palivo se zpočátku uvažuje deuterium a tritium. Deuterium lze izolovat z běžné vody a je možné jej tedy běžně vyrábět. Uvažují se dvě hlavní možnosti - vstříknutí rychlého plynného deuteria a vstřelení pelety deuteria v pevném skupenství. V prvním případě by se plyn primárně zachytil ve svrchní vrstvě plazmatu, ve druhém by se peleta dostala do centra. Tritium není stabilní izotop a v přírodně se prakticky nevyskytuje. Lze jej však získat jadernou reakcí lithia s neutronem. Aktuálně probíhá výzkum o možnosti generování lithia přímo v tokamaku za využití fúzních neutronů.

Kromě doplňování paliva je třeba na tokamaku řešit také sběr částic - unikajících částic a fúzních produktů. Na starších tokamacích se využívá tzv. limiter - speciální mechanická clonka, do které částice prostě narazí místo kolize se stěnou komory. Moderní tokamaky používají tzv. divertor. Ten počítá se speciální konfigurací magnetického pole, kde pouze vnitřní magnetické povrchy jsou uzavřené. Existuje tam tzv. poslední uzavřený magnetický povrch (angl. LCFS), na kterém leží tzv. X-bod. V něm je nulová poloidální složka magnetické indukce. Částice, které se dostanou za tento bod, již nejsou drženy v magnetickém poli a tím se selektivně dostávají z tokamaku pryč. Divertorem se pak nazývá konstrukce, na kterou tyto částice dopadají \footnote{Občas se divertor nazývá celý tokamak s divertorem.}. Ilustrace je na Obr. \ref{fig:divertor}.

\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.6\textwidth]{img/asdex_divertor.png}
	\end{center}
	\caption{Nákres magnetických povrchů a divertoru ASDEX Upgrade, převzato z \cite{asdex_divertor}}
	\label{fig:divertor}
\end{figure}


% subsection pracovní_plyn_a_palivo (end)

% subsection elektromagnetické_pole_v_tokamaku (end)



\subsection{Řízení} % (fold)
\label{sub:řízení}
Prakticky všechny dnes provozované tokamaky jsou řízené pomocí počítačů a mají sběr dat do počítačových databází. Konkrétní implementace se na různých experimentech liší.

Jeden z nejpoužívanějších systémů pro řízení fyzikálních experimentů je EPICS. \cite{EPICS_web}. Systém EPICS byl původně vyvinutý pro synchrotron Advaced Photon Source v laboratoři Argonne, dnes jej používá mnoho laboratoří po celém světě. EPICS je kolekce softwarových nástrojů, naprostá většina z nich je open source. EPICS spojuje jednotlivé počítače, jež obsluhují senzory či ovládací technologie do počítačové sítě. Pomocí nástrojů EPICS se naprogramuje software pro jednotlivé tyto počítače (IOC - Input/Output Controler), který pak běží nad klasickým OS (Windows, GNU/Linux), popř. pod real time OS (RTEMS, VxWorks). Tyto IOC pak komunikují pomocí publisher-subscriber schématu. 

Pro tokamak ITER se počítá se systémem CODAC. Systém CODAC je založený na EPICS a sdílí jeho zaákladní principy. Na tokamaku ITER bude CODAC odpovědný i za sběr a ukládání dat. Aktuálně je systém CODAC testovaný na korejském tokamaku KSTAR. \cite{CODAC_KSTAR}

Naprostá většina moderních tokamaků má implementované technologie pro ovládání různých procesů v průběhu výboje v reálném čase (,,real-time control''). Zpravidla bývá instalováno více systémů pro řízení v reálném čase.

Jednoduchý real time systém je tzv. interlock. Interlock zpravidla detekuje binárně základní stav tokamaku v průběhu výboje a podle toho je schopný promptně aktivovat či deaktivovat nějaký systém. Typickým příkladem je hlídání, zda v tokamaku je plazma. Pokud v tokamaku není plazma, odpojí interlock např. ohřev neutrálním svazkem. Pokud by toto nebylo dodrženo, mohlo by dojít k vážnému poškození tokamaku.

Podstatně složitější systémy se používají pro nastavení parametrů pro výše popsané technologie, např. tvar magnetického pole.

EPICS je vhodný pro ,,soft real-time'' - když zpoždění jednotlivého dílčího kroku řízení (výpočet, předání zprávy) není absolutně kritické. Je přitom třeba uvážit časové škály jevů plazmatu v tokamaku. Obecně lze odhadnout, že pro většinu technologií se potřeby na odezvy jednotlivých součásti systému pohybují v řádech desetin milisekund. To je na hranici systému EPICS provozovaném nad běžnou počítačovou sítí ethernet.


Pro tokamak ITER ja navrženo speciální řešení, kdy budou jednotlivé IOC propojeny speciální 10gbit ethernetovou sítí. Jsou oděěleny sítě pro časování a pro přenos dat pomocí VLAN.\cite{LIU20186} Jednotlivé body komunikují pomocí multicast UDP. \cite{MARTe_eval} \cite{ITER_development_status}.

Nejrychlejší výpočty a zpracování jsou prováděny na jednom počítači. Pro potřeby výzkumu termojaderné fúze byl vyvinutý framework MARTe (Multithreaded Application Real Time executor). Ten usnadňuje vývoj softwaru pro řízení ,,hard real-time''. Framework MARTe je používaný třeba na tokamaku JET i na tokamaku COMPASS na ÚFP AVČR pro řízení stabilizace plazamtu na obou zažízeních pracuje ve smyčce s periodou 50 $\mu$s. \cite{MARTe_compass_1}. MARTe umožňuje flexibilně testovat a vyvíjet algoritmy např. pro výpočet diagnostiky v reálném čase.\cite{JANKY2014186}.

Pro tokamak ITER byly navrženy speciální počítače (i hardware) pro hard real-time řízení. \cite{MARTE-pc}. Schéma řídicího systému je na Obr. \ref{fig:CODAC}.

\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{img/ITER_CODAC.jpg}
	\end{center}
	\caption{Schéma plánovaného řídicího systému tokamaku ITER, převzato z \cite{ITER_development_status}.}
	\label{fig:CODAC}
\end{figure}


% subsection řízení (end)


\subsection{Historie a současnost} % (fold)
\label{sub:historie_a_současnost}
První experimentální zařízení trochu podobné tokamaku sestavili A. Kantrowitz a E. Jacobs na konci 30. let 20. století. \cite{historie_knizka} První tokamak jako vědecký projekt (MTR) byl zkonstruován v Sovětském svazu. Tam již od začátku padesátých let probíhaly experimenty s magnetickými nádobami. Za první tokamak se považuje zařízení TMP (Toroid v magnetickém poli) z roku 1955. Jeho keramická nádoba však neumožňovala mít dost čisté plazma a TMP nedosáhl dobrých výsledků. V roce 1957 byl zkonstruován tokamak T-1, který připomínal konkurenční (americké) zařízení - pinč ZETA. Lišil se podélným magnetickým polem. I ten však trpěl na nestability plazmatu. Na novějším tokamaku T-2 již byly zabudovány technologie vypékání (pro lepší čistotu komory) a limiteru. Průlom způsobil jeho nástupce T-3. Ten byl schopný udržet plazma o hustotě $5 \cdot 10^{19} \mathrm{m}^{-3}$ plazma o elektronové teplotě $8 \cdot 10^6$ K. Přinesl překvapivě příznivé výsledky - obzvlášť malé ztráty energie zářením. V roce 1969 byla díky T-3 v podstatě zahájena mezinárodní spolupráce na výzkumu fúze. Spousta jaderných laboratoří se po úspěchu T-3 vydalo cestou tokamaku na úkor třeba stelarátorů. 
Tokamaky se dodnes zdají být nejnadějnější cestou pro dosažení energeticky využitelné fúze.

Aktuálně jsou v provozu desítky tokamaků po celém světě. Největší tokamak dnes v provozu je JET. Jedná se o celoevropský výzkumný projekt, tokamak JET stojí v Británii.

V ČR byly donedávna v provozu dva tokamaky: GOLEM na FJFI ČVUT a COMPASS na ÚFP AVČR. Tokamak COMPASS nyní bude procházet značnou přestavbou prakticky celého zařízení.

Největší aktuálně probíhající projekt je tokamak ITER, který se právě staví ve Francii. Momentálně není v plánu dosáhnout zapálení na tokamaku ITER\footnote{i když to bylo v původním plánu}. Počítá se však s faktorem zesílení výkonu $Q=10$.

Dále probíhá plánování zařízení DEMO - fúzní elektrárny. Zde se již plánuje reálná energetická využitelnost.
% subsection historie_a_současnost (end)



% section úvod (end)
\clearpage

\subsection{Tokamak GOLEM} % (fold)
\label{sec:golem}
\textit{Experimentální část této práce byla provedena na tokamaku GOLEM. V rámci této práce budu používat označení GOLEM pro celý tokamak, Golem pro bájnou postavu a golem pro lokální označení řídicího počítače tokamaku GOLEM.}


Na počátku šedesátých let byl v Sovětském svazu postaven tokamak s kruhovým průřezem TM-1 (tokamak malyj). Byl to jeden z prvních funkčních tokamaků vůbec. Do roku 1976 byl provozován v Ústavu atomové energie I. V. Kurčatova v Moskvě. Poté byl na základě smlouvy o spolupráci předán do Ústavu fyziky plazmatu Akademie věd ČR. V roce 1977 - 1984 tam byl provozován pod jménem TM1-MH (Microwave Heating). Později bylo upgradováno mnoho systémů (nová komora, magnetický stabilizační systém...). To umožnilo zněkolikanásobit délku výboje. Nový tokamak se od roku 1985 provozoval pod jménem CASTOR (Czech Academy of Sciences TORus). Mezi světovými zařízeními byl zajímavý unikátními diagnostickými systémy pro výzkum v oblasti interakce mikrovln s plazmatem:
\begin{itemize}
	\item VUV spektrometr
	\item XUV spektrometr
	\item mikrovlnný reflektometr
	\item RF sondy až do 10 GHz
\end{itemize}
Od roku 2006 byl na Ústav fyziky plazmatu instalovaný (původně britský) tokamak COMPASS a tokamak CASTOR byl přesunut na FJFI ČVUT a přejmenován na GOLEM. Je pojmenovaný podle mystické postavy Golema, která disponovala ohromnou energií. Tokamak GOLEM je umístěn v budově FJFI Břehová, poblíž židovského hřbitova, kde je pohřben rabbi Löw, tvůrce Golema a podle bájí i samotný Golem. \cite{Castor_history}

% subsection magnetické_udržení (end)
Tokamak GOLEM je aktuálně využívaný jako vzdělávací zařízení. Jeho výhodou nejsou parametry výboje (ty má spíše horší), ale jeho jednoduchost. Lze na něm např. vyvíjet systémy vzdáleného řízení, nebo testovat sondy do plazmatu.
% subsection historie (end)

GOLEM se řadí mezi menší tokamaky. Jeho základní parametry jsou \cite{golem_wiki}:
\begin{itemize}
	\item Hlavní poloměr komory $R_\mathrm{0}=0.4$ m
	\item Vedlejší poloměr komory $r_\mathrm{0} = 0.1$ m
	\item Vedlejší poloměr plazmatu (poloměr limiteru)$a \approx 0.06$ m
	\item Toroidální magnetické pole $B_\mathrm{t} < 0.5$ T
	\item Proud plazmatem $I_\mathrm{p} < 10$ kA
	\item Elektronová teplota $T_\mathrm{e} < 100$ eV
	\item Iontová teplota $T_\mathrm{i} < 50$  eV
	\item Doba výboje $\tau_\mathrm{p} < 30$ ms
	\item Doba udržení elektronů $\tau_\mathrm{e} \approx 50$ \textmu s
\end{itemize}
% subsection základní_charateristiky (end)


\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.7\textwidth]{img/GOLEM_foto.jpg}
	\end{center}
	\caption{Fotografie tokamaku GOLEM. Převzato z \cite{golem_wiki}, upraveno}
	\label{fig:golem_foto}
\end{figure}

Inženýrské schéma tokamaku GOLEM je na Obr. \ref{fig:golem_inzenyrske_schema}.

Toroidální magnetické pole $B_\mathrm{t}$ je generované sadou měděných cívek. Elektrické pole je generované transformátorovým jádrem a sadou měděných cívek primárního vinutí. Cívky jsou na tokamaku GOLEM napájeny z baterie kondenzátorů. Při výboji jsou cívky přímo sepnuté na kondenzátory pomocí výkonových tyristorů. Není tedy možnost pokročilého řízení proudu. Při připojení vzniká RLC obvod\footnote{Se sériovým odporem v řádu jednotek \textohm \  a tedy podstatně podkritickým tlumením } a průběh proudu odpovídá polovině periody harmonického průběhu. 

Komora je evakuovaná pomocí systému rotační vývěvy a dvou turbomolekulárních vývěv. Pracovní plyn byl v minulé verzi napouštěn pomocí motorového jehlového ventilu, nově byl nainstalován elektrický piezoventil.

Dále se na tokamaku GOLEM používá předionizace: žhavená elektroda na elektrickém potenciálu $\approx 300$V proti komoře.

Jako základní parametry výboje lze nastavit
\begin{itemize}
	\item napětí na kondenzátorech pro cívky toroidálního pole $U_\mathrm{B_T}$
	\item napětí na kondenzátorech pro primární vinutí $U_\mathrm{CD}$
	\item čas sepnutí cívek toroidálního pole $T_\mathrm{B_t}$
	\item čas sepnutí cívek primárního vinutí $U_\mathrm{CD}$
	\item pracovní plyn (H / He)
	\item tlak pracovního plynu
	\item předionizace (zapnutá/vypnutá)
\end{itemize}

\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.7\textwidth]{img/inz_schema.png}
	\end{center}
	\caption{Inženýrské schéma tokamaku  GOLEM. Převzato z \cite{golem_wiki}, upraveno}
	\label{fig:golem_inzenyrske_schema}
\end{figure}

Základní diagnostiky na tokamaku GOLEM jsou:
\begin{itemize}
	\item napětí na závit $U_\mathrm{loop}$
	\item toroidální magnetické pole $B_\mathrm{t}$
	\item proud plazmatem $I_\mathrm{p}$
\end{itemize}

\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.7\textwidth]{img/basic_diag.png}
	\end{center}
	\caption{Graficky zpracované základní diagnostiky tokamaku golem, pro výboj 32800, \cite{GM_shot_db}}
	\label{fig:golem_basic_diag}
\end{figure}

$U_\mathrm{loop}$ odpovídá napětí indukovanému na jednom závitu vodiče, který prochází transformátorovým jádrem stejně jako plazma. Odpovídá energii, kterou získá částice náboje 1e při jednom toroidálním oběhu (v eV). Měří se přímočaře - snímá se napětí na drátu nataženém podél komory.

$B_\mathrm{t}$ se na tokamaku GOLEM měří pomocí měřicí cívky - solenoidu s osou rovnoběžnou s vedlejší osou tokamaku. Napětí na této cívce odpovídá časové derivaci velikosti $B_\mathrm{t}$. $B_\mathrm{t}$ se počítá jako
\begin{equation}
	B_\mathrm{t}(t) = L_\mathrm{m} \cdot \int_\mathrm{0}^\mathrm{t} U_\mathrm{m}(\tau)\mathrm{d}\tau
\end{equation}
kde $U_\mathrm{m}$ je napětí na měřicí cívce a $L_\mathrm{m}$ je její indukčnost.

Proud plazmatem $I_\mathrm{p}$ je na tokamaku GOLEM měřený pomocí Rogowského pásku. Ten měří magnetické pole generované proudem, který protéká vodičem.\footnote{opět po integraci, jako v případě měření $B_\mathrm{t}$.}  Rogowského pásek je natažen okolo komory, měří tedy souhrn proudů protékající plazmatem a komorou. V rámci základního zpracování je signál z proudu komorou odečten.

Výstupy z diagnostik je vždy ve formě časového průběhu napětí. Ty jsou sbírány pomocí tzv. DAS - Data Acquisition Systems. To jsou zařízení s rychlými analogově digitálními převodníky (typicky stovky až tisíce ks/s), která jsou schopná zaznamenat během výboje data a po výboji je odeslat do řídicího počítače. Na tokamaku GOLEM se používají primárně tyto DAS:
\begin{itemize}
	\item Osciloskopy Tektronix 5 Series MSO
	\item Různé modely osciloskopů Rigol
	\item Systém PAPOUCH DAS 1210 \cite{golem_wiki}
	\item Počítače s měřicím hardwarem National Instruments
\end{itemize}

Výstup základních diagnostik výboje na tokamaku GOLEM je na Obr. \ref{fig:golem_basic_diag}. Mezi další diagnostiky patří třeba intenzita záření ve viditelném spektru a $\mathrm{H}_\mathrm{\alpha}$ a také interferometrické měření hustoty plazmatu. Ze základních diagnostik je možné spočítat příkon ohmického ohřevu $P_\mathrm{\Omega}$, vodivost plazmatu (elektronovou teplotu $T_\mathrm{e}$), dobu udržení $\tau$ i okrajový bezpečnostní faktor $q_\mathrm{edge}$.


Plazma se obecně makroskopicky chová jako kontinuum a je tedy vhodné jednotlivé vlastnosti popsat skalárním polem/funkcí více proměnných. Pro zpracování je potřeba získat zjednodušený popis nejvýše několika parametry.

Na zařízení typu tokamak platí pro částice plazmatu, že (v toroidálních souřadnicích) převažuje toroidální složka rychlosti (oběh podle hlavní osy). V této práci budou vlastnosti plazmatu uvažovány neměnné pro toroidální otáčení.
Poloha plazmatu by tedy měla být určena 2D profilem hustoty plazmatu. Dále lze učinit zjednodušení, že profil hustoty plazmatu bude radiálně symetrický s posunutím středu proti vedlejší ose tokamaku.\footnote{Tento předpoklad není obecně splněný. Existují např. trajektorie částic pouze podle LFS, tzv. banánové \cite{wesson}. Pro malé tokamaky velikosti GOLEMa může platit relativně dobře.}
S těmito zjednodušujícími předpoklady pak lze parametrizovat polohu plazmatu pomocí dvou skalárních hodnot - horizontální a vertikální souřadnice ,,těžiště'' průřezu plazmatu.


Na tokamaku GOLEM je instalováno několik diagnostických systémů, které umožňují určit polohu plazmatu. Jedna možnost je založená na tom, že plazma je horké a tedy že vyzařuje EM vlny. Při dosahovaných teplotách plazmatu (elektronová teplota v desítkách eV, většinou pod 50 eV) vyzařuje plazma od IR světla až po UV.

Na GOLEMu je instalovaný systém vysokorychlostních kamer \cite{:/content/aip/journal/rsi/83/10/10.1063/1.4731003}, který pomocí rychlého snímání za využití tzv. rolling shutter efektu umožňuje snímat 1D profil vyzařování plazmatu v 12 000 vzorcích za sekundu. CMOS snímač fotoaparátu je nejvíce citlivý přibližně v oblasti viditelného spektra. 

Na tokamaku je dále instalované diodové pole AXUV20EL, kterému se občas ne zcela správně říká bolometry \cite{LeitlMT}. To je namontované přes štěrbinu, která vytváří ,,camera obscura'' (na Obr. \ref{fig:camera_obscura}). To je citlivé až do vzdáleného UV záření. Výstupní signál z bolometrů je 20 kanálů, které pak odpovídají profilu vyzařování plazmatu pro daný směr.

\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.35\textwidth]{img/camera_obscura.png}
	\end{center}
	\caption{Camera obscura pro bolometrická měření na tokamaku GOLEM. Převzato z \cite{LeitlMT}.}
	\label{fig:camera_obscura}
\end{figure}


Další možnost určování polohy je z magnetického pole, které generuje elektrický proud procházející plazmatem. 
Magnetické pole vytvořené pohybujícími se náboji popisuje Biotův-Savartův-Laplaceův zákon. V přiblížení tenkého rovného vodiče, jímž prochází elektrický proud $I$ jej lze zapsat pro magnetickou indukci $\vec B$ v místě $\vec r$
 %BS zakon presunut, musim to fixnout
kde $\vec r_q$ je úsek vodiče.

Pro plazma v tokamaku jej lze zapsat 
\begin{equation}
	B_\theta (r) = \frac{\mu I}{2 \pi r}
\end{equation}
Tento vztah platí při $r > a$, kde $a$ je poloměr válce plazmatu. $B_\theta$ je pak skalární velikost magnetické indukce, směr bude vždy poloidální.

Na tokamaku GOLEM je pro určování polohy instalovaná čtveřice Mirnovových cívek ve stínu limiteru. Mirnovovy cívky jsou orientovány svou magnetickou osou poloidálně a tak měří časovou změnu poloidálního magnetického pole. Jejich časový integrál tedy odpovídá časovému vývoji $B_\theta$. Nákres je na Obr. \ref{fig:mirnov}.
Vývoj časového posunu prstence plazmatu by bylo možné počítat ze signálu z jedné Mirnovovy cívky při znalosti velikosti proudu plazmatem. Také je možné počítat vývoj polohy plazmatu ze signálu z dvou protilehlých cívek, což se na GOLEMu používá. \cite{Jindra_DP}
\begin{figure}[!ht]
	\begin{center}
		\includegraphics[width=0.55\textwidth]{img/mirnov_coils.png}
	\end{center}
	\caption{Cívky pro měření poloidálního magnetického pole na GOLEMu. Převzato z \cite{Jindra_DP}.}
	\label{fig:mirnov}
\end{figure}
Situace je podstatně jednodušší pro vertikální směr - není třeba uvažovat toroidální zakřivení plazmatu. Posunutí prstence plazmatu ve vertikálním směru $\Delta_\mathrm{Z}$ lze spočítat
\begin{equation}
	\Delta_\mathrm{Z}=\frac{B_{\theta=0}-B_{\theta=\pi}}{B_{\theta=0}+B_{\theta=\pi}} \cdot b
\end{equation}
kde $b$ je vzdálenost cívek od středu komory a $\Theta$ je poloidální úhel kde 0 odpovídá směru kolmo nahoru.
Určení polohy plazmatu v horizontálním směru pomocí magnetické diagnostiky je značně komplikovanější.\cite{Jindra_DP}


Pro měření hustoty plazmatu se využívá mikrovlnné měření. Zjednodušeně lze říci, že EM vlna se plazmatem propaguje, pokud její úhlová frekvence $\omega$ je větší, než plazmová frekvence $\omega_\mathrm{p}$. Plazmovou frekvenci lze spočítat
\begin{equation}
	\omega_\mathrm{p} ^2 = \frac{n e^2}{\varepsilon_0 m} 	
\end{equation}
kde $n$ je hustota plazmatu a $m$ je hmotnost elektronu.

Pro vlny, které podmínku $\omega > \omega_\mathrm{p}$ splňují, lze vyjádřit jejich index lomu v plazmatu $N_\mathrm{O}$ \cite{LMatena_DP}
\begin{equation}
	N_\mathrm{O} = \sqrt{1- \frac{\omega_\mathrm{p}^2}{\omega ^2}} \approx 1- \frac{n e^2}{2 \varepsilon_0 m \omega ^2}
\end{equation}
Samotné měření pak probíhá interferometricky. Využívá se Gunnova dioda jako zdroj mikrovln. Záření je vedeno vlnovody a rozdvojeno. Jedno rameno cesty vede skrz komoru tokamaku. Vyhodnocovací zařízení pak určuje fázový posun vlny přímé a vlny jdoucí skrz plazma. Nákres interferometrického měření je na Obr. \ref{fig:interferometry}.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.8\textwidth]{img/LMatena_interferometry.png}
	\end{center}
	\caption{Schéma systému interferometrického měření hustoty plazmatu. Převzato z \cite{LMatena_DP}.}
	\label{fig:interferometry}
\end{figure}


Na tokamaku GOLEM nejsou Mirnovovy cívky orientovány dokonale přesně a proto je v signálu z cívek patrné i toroidální magnetické pole. Jelikož je velikost toroidálního pole vyšší, nelze signál toroidálního pole zanedbat.

Na některých tokamacích byly zkoušeny i další způsoby určování polohy plazmatu pro řízení v reálném čase, třeba pomocí elektrostatických sond. \cite{poloha_langumir} Na GOLEMu tato technologie není implementovaná.


Scénář výboje na tokamaku GOLEM je sekvence fází
\begin{enumerate}
	\item přípravná fáze (nabíjení kondenzátorů, napouštění plynu, předionizace)
	\item arming (příprava datových sběrů)
	\item trigger sekvence (samotný výboj, sběr dat)
	\item povýbojová fáze (deaktivace systémů, stažení, zpracování a uložení dat)
\end{enumerate}


Tokamak GOLEM prošel v akademickém roce 2017/18 značným upgradem samotného experimentu. Tato práce vznikala v letech 2018/2020. Během této doby prodělal řídicí systém tokamaku veliké změny, na kterých spolupracovalo více lidí. V této kapitole je zmíněn minulý stav a rozebrán systém nový, který je v momentě psaní této práce rozpracovaný.

Starý i nový řídicí systém je založený na ethernetové síti.
Starý řídicí systém byl centralizovaný, prakticky všechny věci obstarával jeden hlavní počítač (golem).
Probíhající upgrade přináší částečnou decentralizaci. Jeden řídicí počítač pořád zůstane, ale nebude již vykonávat všechny částečné úlohy.

Na hlavním počítači tokamaku běží operační systém GNU/Linux. Na hlavním počítači běží démon, který prochází frontu požadovaných výbojů a následně je realizuje.

V aktuálním stavu je pro každé externí zařízení na golemu jedna složka a v ní Makefile soubor pro GNU/make. Zařízení se ovládají každý výboj pomocí tzv. kotev. Ty jsou momentálně dány jako cíle v makefile:

\begin{enumerate}
	\item \texttt{pre\_discharge}
	\item \texttt{arming}\\
	\textit{samotný výboj}
	\item \texttt{post\_discharge}
\end{enumerate}


Momentálně není systematicky řešené, jak v rámci makefile předávat větší množství dat jako argumenty (waveformy).


Pro oblast této práce, řízení v reálném čase, je stěžejní systém časování a triggerů. V experimentální fyzice často triggery znamenají systém, který vyhodnocuje a vybírá data v reálném čase. Na GOLEMu triggery znamenají jen generátor synchronizačních signálů.
Ethernetová síť mezi více počítači není časově přesná. Doba odezvy mezi dvěma zařízeními propojenými switchem může fluktuovat zpravidla v desetinách milisekundy. Řízení přes ethernetovou síť lze tedy bez problémů použít např. pro ovládání nabíjení kondenzátorů, ale ne  pro spínání tyristorů.
Pro takové děje se na GOLEMu využívá synchronizační TTL signál vedený koaxiálními kabely - triggery.

Původní systém generování triggerů sestával z čtveřice proprietárních zařízení, která umožňovala nastavit přesně vzájemné zpoždění čtyř binárních signálů a poté jejich spuštění. Výhodou tohoto systému byla nenáročná implementace přesného časování. Nevýhodami našeho konkrétního modelu jsou pak např. nemožnost použití více kanálů, než čtyř a problematická nahraditelnost (už se nevyrábí).

Nový systém je aktuálně ve vývoji a bude založen na osmibitovém mikroprocesoru Atmel ATMega a vlastní elektronice. Na tokamaku GOLEM jsou všechny výstupy generátoru triggerů galvanicky oddělené pomocí optočlenu. Počítá se s tím, že generátor triggerů bude přímo otevírat tyristor.
Ve starém systému se počítalo napevno se dvěma kanály pro spínání tyristorů pro magnetické a pro elektrické pole, třetí byl rezervovaný pro elektrické pole pro moment průrazu a čtvrtým se řídily datové sběry. V novém systému bude k dispozici 7 kanálů.


Ve starém i novém systému je tokamak GOLEM evakuován systémem rotační olejové vývěvy a dvou turbomolekulárních vývěv. Vývěvy jsou řízené elektronicky, připojování ke komoře je pak řešené pomocí systému pneumaticky řízených ventilů s širokým průchodem.

Měření tlaku je pak prováděno pomocí jedné dvojité vakuové měrky Pfeiffer Vacuum PKR 251. Jedna měrka v sobě obsahuje dva subsystémy: Piraniho měrku pro vyšší tlaky a měrku se studenou katodou pro nižší tlaky. Vyhodnocení obstarává originální vyčítací zařízení a výstup je již tlak v pascalech. \cite{vacuum_gauge}

Na tokamaku GOLEM se jako pracovní plyn nejčastěji používá buď vodík, nebo helium.\footnote{Nově se uvažuje i o deuteriu či argonu.} Plyn je uschován v tlakových lahvích (kvůli bezpečnosti s manuálním ventilem) a napouštěn soustavou redukčního ventilu a ve starém systému počítačem řízeného jehlového ventilu.
V přípravné fázi je na počítači spuštěna řídicí smyčka. Napouštění se dostane do rovnováhy s čerpáním vývěvami. Tlak pracovního plynu redukčního ventilu a ve starém systému počítačem řízeného jehlového ventilu se ustálí na požadované hodnotě  $p_{wg} \approx 10 - 200$ mPa. Po celou dobu výboje zůstávala rychlost napouštění konstantní.
V novém systému byl instalovaný počítačem řízený piezoventil, který pracuje ve stejném režimu.


Zásadním problémem na tokamaku GOLEM byla absence řízení experimentu v reálném čase. V průběhu výboje se plazma hýbe a posouvá. Mechanismy, proč se to tak děje, jsou různé. Každá nedokonalost magnetického pole může způsobovat drift plazmatu. Dále se při výboji poruší rovnováha napouštění - ionizované částice se zachytí na limiteru či na komoře a hustota plazmatu v průběhu času klesá.  Na jiných tokamacích je často možné v reálném čase řídit magnetické i elektrické pole, vstřikování plynu (a tedy hustotu plazmatu) i různé metody ohřevu.



Na tokamaku GOLEM je možné realizovat výboj i bez řízení v reálném čase tedy bez stabilizace i vstřikování (gas puffing).
Nepřítomnost těchto systémů ovšem značně omezuje možnosti experimentů a proto je vhodné tyto systémy implementovat.

V rámci starého systému byl na tokamaku GOLEM systém stabilizace polohy plazmatu, který mohl operovat v režimu předdefinovaného zásahu i zpětnovazebního řízení. \cite{Jindra_BP,Jindra_DP,Svoboda2015}
Ten byl postaven na speciálním rychlém počítači National Instruments PXI. Pro zjišťování polohy plazmatu využíval magnetické diagnostiky - jeden směr polohy plazmatu byl určen z dvojice Mirnovových cívek.


% subsection golem (end)

\clearpage