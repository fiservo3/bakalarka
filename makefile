all: pdf bib
	#Make se snazi byt chytrej a nepousti ho znova
	#takze mu to musim napsat jak pro blbecka, protoze TeX se tak nechova
	pdflatex BP.tex
	pdflatex BP.tex
	#otevre soubor
	xdg-open BP.pdf

pdf:
	pdflatex BP.tex

bib:
	bibtex BP

clean: 
	rm -f *.aux
	rm -f *.blg
	rm -f *.fdb_latexmk
	rm -f *.log
	rm -f *.out
	rm -f *.synctex.gz
	rm -f *.fls
	rm -f *.bbl

	rm -f BP.pdf
