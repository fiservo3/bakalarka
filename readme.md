# Moje bakalářská práce na FJFI
## téma řízení tokamaku v reálném čase

Obrázky jsou ve složce img. Nejsem autorem ani vlastníkem práv všech použitých obrázků. Původní obrázek je vždy ozdrojován v textu bakalářské práce: 
```
	BP.pdf
```
.

# Překlad

používám pdflatex pro překlad a bibtex pro reference, překlad by mělo jít pustit

```
	pdflatex BP.tex
	bibtex BP
	pdflatex BP.tex
	pdflatex BP.tex
```

Napsal jsem jednoduchý makefile, pro překlad by mělo stačit pustit

```
	make
```

Já to překládal pod linuxem, distribuce LaTeXu TeXlive.
Výstupní soubour by měl být: 
```
	BP.pdf
```

